var canvas = document.querySelector('canvas');


var c = canvas.getContext('2d');

var dimension = 16;

var q = new Image();
q.src = 'queen.jpg';


var x = new Array(dimension);

for (var i = 0; i < dimension; i++) {
  x[i] = new Array(dimension);
}
x[1][1]=20;
console.log(x[1][1]);



drawGrid(dimension,60,"black","white");
img1.onload = function () {
    //draw background image
    c.drawImage(img1, 0, 0);
    //draw a box over the top
    c.fillStyle = "rgba(200, 0, 0, 0.5)";
    c.fillRect(0, 0, 500, 500);

};

function drawGrid(gridLength,factor,color1,color2) {
    canvas.width=gridLength*factor;
    canvas.height=gridLength*factor;
    var x = 0;
    var y = 0;
    var elements = gridLength*gridLength;
    var line = 0;
    for (var i = 0; i < elements; i++) {
        if(i % Math.sqrt(elements)==0 && i != 0){
            y = y+factor;
            x = 0;
            line = !line;
        }
    
        if(i %2 == line ){
            c.fillStyle = color1;
        }else{
            c.fillStyle = color2;  
        }
    
        c.fillRect(x,y,factor,factor);
        x = x+factor;
    }
}